from django.conf import settings
from django.urls import include, path
from rest_framework.routers import DefaultRouter, SimpleRouter

from idp.core.users.views import UserViewSet

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

router.register("", UserViewSet)


app_name = "users"

ver_1_patterns = [
    path('', include(router.urls)),
]

urlpatterns = [
    path('v1/', include(ver_1_patterns)),
]
